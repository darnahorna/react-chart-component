import React, { useState } from "react";
import Column from "./Column";
import { data } from "./data";

export default function Chart() {
  return (
    <div className="Chart">
      <h2>SPENT TIME (SECONDS)</h2>
      <Column data={data} />
    </div>
  );
}
