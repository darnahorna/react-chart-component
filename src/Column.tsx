import React from "react";
import "./Column.css";

export default function Column(props: any) {
  //console.log(data);
  // console.log(props.data);

  return (
    <div className="Column">
      {props.data.map((element: any, i: number, time: number) => (
        <div className="parent  " key={i}>
          <div className="label ">{element.name}</div>

          <div className="time">
            <input
              className="range"
              type="range"
              min="0"
              max="18.4"
              value={(time = +element.time)}
              step="0.1"
              readOnly
            />
          </div>
        </div>
      ))}
    </div>
  );
}
